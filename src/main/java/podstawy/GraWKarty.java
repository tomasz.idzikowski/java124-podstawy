package podstawy;

import java.util.ArrayList;
import java.util.List;

public class GraWKarty {
    public static void main(String[] args) {
        KartaDoGry damaPik = new KartaDoGry(Figura.DAMA, Kolor.PIK);
        KartaDoGry krolKaro = new KartaDoGry(Figura.KROL, Kolor.KARO);
        KartaDoGry krolPik = new KartaDoGry(Figura.KROL, Kolor.PIK);
        KartaDoGry asPik = new KartaDoGry(Figura.AS,Kolor.PIK);

        KartaDoGry damaTrefl = new KartaDoGry(Figura.DAMA, Kolor.TREFL);
        KartaDoGry krolKier = new KartaDoGry(Figura.KROL, Kolor.KIER);

        List<KartaDoGry> kartyGracza = new ArrayList<>();
        kartyGracza.add(damaPik);
        kartyGracza.add(krolKaro);
        kartyGracza.add(krolPik);
        kartyGracza.add(asPik);

        List<KartaDoGry> kartyKomputera = new ArrayList<>();
        kartyKomputera.add(damaTrefl);
        kartyKomputera.add(krolKier);

        System.out.println("Przed rozpoczęciem gry");
        System.out.println("Karty gracza "+kartyGracza);
        System.out.println("Karty komputera "+kartyKomputera);
        while (kartyGracza.size()!=0 && kartyKomputera.size()!=0){
            System.out.println("Karty na stół");
            KartaDoGry kartaGracza = kartyGracza.get(0);
            KartaDoGry kartaKomputera = kartyKomputera.get(0);
            System.out.println("Trwa walka");
            System.out.println("Gracz połozył "+kartaGracza);
            System.out.println("Komputer połozył "+kartaKomputera);
            if (kartaGracza.compareTo(kartaKomputera)<0){
                System.out.println("Gracz przegrał bitwę");
                kartyGracza.remove(0);
                kartyKomputera.remove(0);
                kartyKomputera.add(kartaKomputera);
                kartyKomputera.add(kartaGracza);
            }
            else {
                System.out.println("Komputer przegrał bitwę");
                kartyKomputera.remove(0);
                kartyGracza.remove(0);
                kartyGracza.add(kartaGracza);
                kartyGracza.add(kartaKomputera);
            }
            System.out.println("Po kolejnej bitwe");
            System.out.println("Karty gracza "+kartyGracza);
            System.out.println("Karty komputera "+kartyKomputera);
        }
        if (kartyGracza.isEmpty()){
            System.out.println("Przegrał gracz");
        }
        else {
            System.out.println("Przegrał komputer");
        }
    }
}
